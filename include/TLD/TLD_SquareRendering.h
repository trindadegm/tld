/*
MIT License

Copyright (c) 2018, 2019 Gustavo Moitinho Trindade

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef INCLUDE_TLD_SQUARERENDERING_H_
#define INCLUDE_TLD_SQUARERENDERING_H_

#include <TLD/TLD_RenderContext.h>
#include <TLD/TLD_TileMap.h>
#include <TLD/TLD_Texture.h>
#include <SDL2/SDL.h>

/*
 * Draws a tile map on the current context.
 * 
 * Parameters:
 *   TLD_RenderContext *renderContext: The rendering context.
 *   SDL_Renderer *renderer: Where to render.
 *   const TLD_TileMap *tileMap: The tile map to be rendered.
 * 
 * SDL video subsystem shall be loaded, otherwise this function may not behave
 * properly.
 *
 * A null tile is represented by -1 (or TLD_NO_TILE). When trying to draw the
 * tile -1, it will leave the space empty (will NOT drawn on it) and jump to the next tile.
 * 
 * Collateral effects:
 * This function does NOT modify the parameter tileMap.
 * This function DOES modify the parameter *renderContext.
 * 
 * All parameters shall be on valid states.
 * 
 * Return value (int):
 *  0: Success
 *  1: Failure
 */
int TLD_SquareRenderCopy(TLD_RenderContext *renderContext, SDL_Renderer* renderer, const TLD_TileMap *tileMap); 

/*
 * Loads a texture from a single file.
 *
 * Parameters:
 *   const char *fileName: A file containing the texture. The file shall contain
 *     all of the sprites needed for rendering of the tile map.
 *   SDL_Renderer *renderer: The SDL_Renderer to which to draw. Needed because
 *     of hardware acceleration (driver specific textures).
 *   int tileWidth: The width of each tile.
 *   int tileHeight: The height of each tile.
 *   int srcColNumber: The number of columns (sprites on y axis) of the source
 *     image.
 *   int tileNumber: The number of tiles.
 *
 * SDL Image PNG subsystem shall be loaded, otherwise this function may not behave
 * properly.
 *
 * All parameters shall be on valid states.
 *
 * Return value (TLD_Texture*):
 *   NULL: Failure
 *   otherwise: The texture is loaded and ready to use
 */
TLD_Texture *TLD_LoadSquareTextureSingleFile(const char *fileName,
                                             SDL_Renderer *renderer,
                                             int tileWidth,
                                             int tileHeight,
                                             int srcColNumber,
                                             int tileNumber);

/*
 * Creates a render context for square tiling rendering.
 *
 * Parameters:
 *   const TLD_Texture *tileTexture: A loaded tile texture.
 *
 * Return value (TLD_RenderContext*):
 *   NULL: Failure
 *   otherwise: The render context loaded and ready to use
 */
TLD_RenderContext *TLD_CreateSquareRenderContext(TLD_Texture *tileTexture);

#endif // INCLUDE_TLD_SQUARERENDERING_H_
