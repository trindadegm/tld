/*
MIT License

Copyright (c) 2018, 2019 Gustavo Moitinho Trindade

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef INCLUDE_TLD_RENDERCONTEXT_H_
#define INCLUDE_TLD_RENDERCONTEXT_H_

#include <TLD/TLD_Texture.h>
#include <SDL2/SDL.h>

typedef int TLD_RenderContextType;

typedef struct TLD_RenderContext
{
  TLD_Texture *texture;
  SDL_Point camera;
  TLD_RenderContextType type;
} TLD_RenderContext;

/*
  Sets the camera location of a render context.

  Parameters:
    TLD_RenderContext *renderContext: The subject render context.
    SDL_Point camera: The new render context's camera location.
*/
static inline void TLD_SetCamera(TLD_RenderContext *renderContext, SDL_Point camera)
{
  renderContext->camera = camera;
}

/*
  Returns the camera location of a render context.

  Parameters:
    TLD_RenderContext *renderContext: The subject render context.

  Return value (SDL_Point):
    SDL_Point p: Where p is the camera point
*/
static inline SDL_Point TLD_GetCamera(TLD_RenderContext *renderContext)
{
  return renderContext->camera;
}

/*
 * Destroys a render context.
 *
 * Parameters:
 *  const TLD_RenderContext* renderContext: The render context to destroy
 */
void TLD_DestroyRenderContext(TLD_RenderContext* renderContext);

#endif // INCLUDE_TLD_RENDERCONTEXT_H_
