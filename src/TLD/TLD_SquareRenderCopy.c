/*
MIT License

Copyright (c) 2018 Gustavo Moitinho Trindade

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <TLD/TLD_SquareRendering.h>

static inline int aux_MaxInt(int a, int b)
{
  return (a > b) ? a : b;
}

static inline int aux_MinInt(int a, int b)
{
  return (a < b) ? a : b;
}

int TLD_SquareRenderCopy(TLD_RenderContext *renderContext, SDL_Renderer* renderer, const TLD_TileMap *tileMap)
{
  TLD_TileSprite *sprites = renderContext->texture->sprites;
  int spriteNumber = renderContext->texture->spriteNumber;
  if (spriteNumber == 0)
  {
    return -1;
  }

  // Get the viewport of the renderer, to know the limits of the frame.
  SDL_Rect viewport;
  SDL_RenderGetViewport(renderer, &viewport);

  // Get tile width and height from first sprite (they should all be equal sized).
  const int tileH = sprites[0].rect.h;
  const int tileW = sprites[0].rect.w;

  const int cameraX = renderContext->camera.x;
  const int cameraY = renderContext->camera.y;

  // Calculate the boundaries defined by the camera and frame limits, this is used
  // so that tiles outside the viewport are not drawn. Only the tiles the will be
  // visible (at least will be on the visible part of the screen) will be drawn. 
  const int cameraRangeMinI = aux_MaxInt(cameraY/tileH, 0); // Never do negative indexing.
  const int cameraRangeMinJ = aux_MaxInt(cameraX/tileW, 0); // Never do negative indexing.

  // Get from which point (on x and y) the tiles should start to be drawn.
  const int xDrawingBegin = cameraRangeMinJ*tileW - cameraX;
  const int yDrawingBegin = cameraRangeMinI*tileH - cameraY;

  // Finishes by getting the maximum index on i (line) and j (column)
  // OBS: The "+ tileH" on the calculation is important to avoid drawing unnecessary tiles.
  // It is not equivalent to (viewport.h + cameraY)/tileH + 1, because the division ("/")
  // in question is integer division, so the result is truncated. With a plus 1, sometimes a
  // tile will be rendered event when it is not on view.
  //
  // Notice that one extra tile is drawn on the occasion of the tile being positioned
  // at the very edge of the screen (the biggest y and x side edges, bottom and right).
  // This was let as is because it does not seems much of problem.
  const int cameraRangeMaxI = aux_MinInt((viewport.h + cameraY + tileH)/tileH, tileMap->height);
  const int cameraRangeMaxJ = aux_MinInt((viewport.w + cameraX + tileW)/tileW, tileMap->width);

  // If has nothing to draw
  if (cameraRangeMinI == cameraRangeMaxI || cameraRangeMinJ == cameraRangeMaxJ)
  {
    return 0;
  }

  int *tiles = tileMap->tiles;
  SDL_Rect dstRect = (SDL_Rect) {.x = xDrawingBegin, .y = yDrawingBegin, .w = tileW, .h = tileH};
  for (int i = cameraRangeMinI; i < cameraRangeMaxI; ++i)
  {
    for (int j = cameraRangeMinJ; j < cameraRangeMaxJ; ++j)
    {
      int tileIndex = tileMap->height*i + j; // Calculate the tile index of the tile to be drawn.
      if (tiles[tileIndex] > -1 || tiles[tileIndex] < spriteNumber)
      {
        SDL_RenderCopy(renderer, sprites[tiles[tileIndex]].texture, &sprites[tiles[tileIndex]].rect, &dstRect);
        dstRect.x += dstRect.w;
      }
    }
    dstRect.x = xDrawingBegin;
    dstRect.y += tileH;
  }
  return 0;
}
