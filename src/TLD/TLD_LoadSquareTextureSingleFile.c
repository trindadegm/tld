/*
MIT License

Copyright (c) 2018 Gustavo Moitinho Trindade

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <TLD/TLD_SquareRendering.h>
#include <SDL2/SDL_image.h>
#include <malloc.h>

TLD_Texture *TLD_LoadSquareTextureSingleFile(const char* fileName,
                                             SDL_Renderer *renderer,
                                             int tileWidth,
                                             int tileHeight,
                                             int srcColNumber,
                                             int tileNumber)
{
  SDL_Surface *image;
  image = IMG_Load(fileName);
  if (!image)
  {
    return NULL;
  }
  else
  {
    SDL_Texture *texture;
    texture = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);
    image = NULL;

    TLD_Texture *tileTexture = malloc(sizeof(TLD_Texture));
    if (tileTexture == NULL)
    {
      SDL_DestroyTexture(texture);
      return NULL;
    }

    tileTexture->spriteNumber = tileNumber;
    tileTexture->sprites = malloc(sizeof(TLD_TileSprite)*tileNumber);
    if (tileTexture->sprites == NULL)
    {
      SDL_DestroyTexture(texture);
      free(tileTexture);
      return NULL;
    }

    SDL_Rect spriteRect = {.x = 0, .y = 0, .w = tileWidth, .h = tileHeight};
    int arrayIndex = 0;
    int srcLineNumber = tileNumber/srcColNumber;
    for (int i = 0; i < srcLineNumber; ++i)
    {
      for (int j = 0; j < srcColNumber; ++j)
      {
        tileTexture->sprites[arrayIndex].texture = texture;
        tileTexture->sprites[arrayIndex].rect = spriteRect;
        spriteRect.x += tileWidth;
        ++arrayIndex;
      }
      spriteRect.x = 0;
      spriteRect.y += tileHeight;
    }

    return tileTexture;
  }
}
