/*
MIT License

Copyright (c) 2018, 2019 Gustavo Moitinho Trindade

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <TLD/TLD.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

SDL_version compiledSDLVersion;
SDL_version linkedSDLVersion;

  int tiles[] =
    {
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
      0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
      0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
      0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
      0,1,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,0,0,3,3,0,0,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,0,3,2,2,3,0,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,3,2,1,1,2,3,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,3,2,1,0,0,1,2,3,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,3,2,1,0,0,1,2,3,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,3,2,1,1,2,3,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,0,3,2,2,3,0,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,0,0,3,3,0,0,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,1,0,
      0,1,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,1,0,
      0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
      0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
      0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    };

int main(void)
{
  printf("TiLDe TLD v0.2 test.\n");

  SDL_VERSION(&compiledSDLVersion);
  SDL_GetVersion(&linkedSDLVersion);
  printf("SDL version %d.%d.%d, linked against version %d.%d.%d.\n",
          compiledSDLVersion.major, compiledSDLVersion.minor, compiledSDLVersion.patch,
          linkedSDLVersion.major, linkedSDLVersion.minor, compiledSDLVersion.patch);

  if (SDL_Init(SDL_INIT_EVERYTHING) > 0)
  {
    printf("[FATAL] ERROR: Failed to initialize SDL; %s\n", SDL_GetError());
    return -1;
  }

  if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
  {
    printf("[FATAL] ERROR: Failed to initialize SDL image; %s\n", IMG_GetError());
    SDL_Quit();
    return -1;
  }

  SDL_Window *window = SDL_CreateWindow("TiLDe tiling test",
                                        SDL_WINDOWPOS_UNDEFINED,
                                        SDL_WINDOWPOS_UNDEFINED,
                                        500,
                                        500,
                                        0);

  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  TLD_Texture *tileTexture = TLD_LoadSquareTextureSingleFile("assets/test_2x2_tiles.png", renderer, 50, 50, 2, 4);
  if (tileTexture == NULL)
  {
    printf("[FATAL] ERROR: Failed to load tile texture; %s\n", IMG_GetError());
    return -1;
  }

  TLD_RenderContext *renderContext = TLD_CreateSquareRenderContext(tileTexture);
  if (renderContext == NULL)
  {
    printf("[FATAL] ERROR: Failed to create a tiled render context.\n");
    return -1;
  }
  TLD_TileMap tileMap;
  tileMap.tiles = tiles;
  tileMap.width = 30;
  tileMap.height = 30;

  SDL_Rect viewport = (SDL_Rect) {.x = 0, .y = 0, .w = 500, .h = 500};
  SDL_RenderSetViewport(renderer, &viewport);


  int exit = 0;
  SDL_Event event;
  while (!exit)
  {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);
    SDL_RenderClear(renderer);
    // Start rendering 

    TLD_SquareRenderCopy(renderContext, renderer, &tileMap);

    // Finish rendering
    SDL_RenderPresent(renderer);

    while (SDL_PollEvent(&event))
    {
      SDL_Point toUpdateCam;
      switch (event.type)
      {
        case SDL_QUIT:
          exit = 1;
          break;
        case SDL_KEYDOWN:
          toUpdateCam = (SDL_Point) {0, 0};
          switch (event.key.keysym.scancode)
          {
            case SDL_SCANCODE_W:
              toUpdateCam.y = -5;
              break;
            case SDL_SCANCODE_S:
              toUpdateCam.y = 5;
              break;
            case SDL_SCANCODE_A:
              toUpdateCam.x = -5;
              break;
            case SDL_SCANCODE_D:
              toUpdateCam.x = 5;
              break;
            default:
              break;
          }
          SDL_Point cam = TLD_GetCamera(renderContext);
          cam = (SDL_Point) {.x = cam.x + toUpdateCam.x, .y = cam.y + toUpdateCam.y};
          TLD_SetCamera(renderContext, cam);
          break;
        default:
          break;
      }
    }

    //SDL_Delay(16);
  }

  SDL_DestroyWindow(window);
  SDL_DestroyRenderer(renderer);
  TLD_DestroyTexture(tileTexture);
  TLD_DestroyRenderContext(renderContext);

  IMG_Quit();
  SDL_Quit();
  return 0;
}
