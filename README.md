# TLD: TiLDe, a simple 2D tile map API for SDL2.

## Features:
- Allows easy rendering of square tiled maps
- Allows easy camera positioning of view over map
- Allows easy loading of png tile texture from single file

## Requisites:
- SDL2 development library (usually `libsdl2-dev`, on debian based systems)
- SDL2 image development library (usually `libsdl2-image-dev`, on debian based systems)
--------------
Should be compiled with a C11 compliant C compiler. Although it was only tested with gcc (6.3.0).
